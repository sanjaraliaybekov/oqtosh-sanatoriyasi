import { Splide, SplideSlide } from "@splidejs/react-splide";
import React, { useEffect, useState } from "react";
import { izohlar } from "../../../../api/requests";

export default function Slider() {
	const [comments, setComments] = useState([]);
	useEffect(() => {
		izohlar().then((res) => {
			setComments(res.data);
		});
    }, []);

	return (
		<div className="container">
			<div className="row">
				<div className="col-12">
					<div className="slider">
						<h1>Mijozlar fikirlari</h1>
						<Splide
							options={{
								type: "loop",
                                drag: "free",
								gap: "20px",
                                perPage: 3,
                                pagination: false,
								autoplay: true,
                                pauseOnHover: true,
								autoScroll: {
									speed: 3,
								},
							}}
						>
							{comments.map((user,i) => (
								<SplideSlide key={i}>
									<div className="box">
										<h2 className="name">{user.name}</h2>
										<h3 className="comment">
											{user.comment}
										</h3>
									</div>
								</SplideSlide>
							))}
						</Splide>
					</div>
				</div>
			</div>
		</div>
	);
}
