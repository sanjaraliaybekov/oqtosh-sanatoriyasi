import React, { useEffect, useState } from "react";
import {Link} from "react-router-dom";
import {motion} from 'framer-motion/dist/framer-motion'
import { blocks } from "../../../../api/requests";



export default function HomeBlocksSection({setAnimateExit}) {
    window.scrollTo({ top: 0, behavior: "smooth" });
    
    const [blocksInfo, setBlocksInfo] = useState([]);
	useEffect(() => {
		blocks().then((res) => {
			setBlocksInfo(res.data);
		});
    }, []);



    return (
        <div className="home-blocks container">
            {blocksInfo.map((item, index) => (
                <div className="row home-block" key={index}>
                    <div className="col-1 home-block__index">
                        {(index + 1).toString().padStart(2, "0")}
                    </div>
                    <div className="col-5 me-5 order-3">
                        <div className="home-block__mini-title with-pre-line">
                            {item.miniTitle}
                        </div>
                        <div className="home-block__title">{item.title}</div>
                        <div className="home-block__content">
                            {item.content}
                        </div>
                        <div className="home-block__link" onClick={() => setAnimateExit(false)}>
                            <Link to={item.link}>Ko'proq ko'rish</Link>
                            <img
                                src="/assets/images/icons/arrow_variant.svg"
                                alt="arrow"
                            />
                        </div>
                    </div>
                    <div className="col-4 order-4 home-block__image">
                        <motion.img
                            transition={{duration: 0.5}}
                            layoutId={item.link}
                            className="w-100"
                            src={item.image}
                            alt={item.image}
                        />
                    </div>
                </div>
            ))}
        </div>
    );
}
