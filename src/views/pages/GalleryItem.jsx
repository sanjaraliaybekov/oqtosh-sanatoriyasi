import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { galereya } from "../../api/requests";
import Header from "../../components/common/headers/header";
import PageAnimation from "../../components/common/PageAnimation";

export default function GalleryItem() {
	let { id } = useParams();
	const [gallery, setGallery] = useState([]);

	useEffect(() => {
		galereya().then((res) => {
			setGallery(res.data);
		});
	}, []);

	return (
		<PageAnimation inEffect={false}>
			<div
				className="d-flex justify-content-center align-items-center galery-item"
				style={{ height: "80vh", width: "100vw", zIndex: 5 }}
			>
				<div className="headere">
					<Header />
				</div>

				<div
					className="head-image"
					style={{
						width: "100vw",
						height: "80vh",
						backgroundPosition: "center",
						backgroundSize: "cover",
						backgroundImage: `url("${gallery[id]?.img}")`,
					}}
				/>
			</div>
			<h2 className="mt-5 container">{gallery[id]?.text}</h2>
		</PageAnimation>
	);
}
