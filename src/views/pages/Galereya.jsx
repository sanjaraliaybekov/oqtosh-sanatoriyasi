import React, { useRef, useEffect, useState } from "react";
import { motion } from "framer-motion/dist/framer-motion";
import { Link } from "react-router-dom";
import { layout } from "../../constants/layouts";
import useMouse from "@react-hook/mouse-position";
import PageAnimation from "../../components/common/PageAnimation";
import { useWindowSize } from "../../utils/useWindowSize";
import { galereya } from "../../api/requests";

export default function Galereya() {
	const [innerOffset, setInnerOffset] = useState([200, 200]);
	const [innerStyle, setInnerStyle] = useState({});
	const [selected, setSelected] = useState(null);

	const wSize = useWindowSize();

	const outer = useRef(null);
	const inner = useRef(null);
	const mouse = useMouse(outer);
	const mouse2 = useMouse(inner);
	const [gallery, setGallery] = useState([]);

	useEffect(() => {
		galereya().then((res) => {
			setGallery(res.data);
		});
	}, []);

	useEffect(() => {
		if (selected === null && wSize.width > 992) {
			setInnerOffset([
				(mouse.pageX / mouse.elementWidth) *
					(mouse2.elementWidth - mouse.elementWidth),
				(mouse.pageY / mouse.elementHeight) *
					(mouse2.elementHeight - mouse.elementHeight),
			]);
			setInnerStyle({
				transform: `translate(-${innerOffset[0]}px, -${innerOffset[1]}px)`,
			});
		}
	}, [
		mouse.pageX,
		mouse.pageY,
		// innerOffset,
		// mouse.elementHeight,
		// mouse.elementWidth,
		// mouse2.elementHeight,
		// mouse2.elementWidth,
		// selected,
		// wSize.width,
	]);
	useEffect(() => {
		setInnerStyle({ transform: `translate(-0px, -0px)` });
	}, [wSize]);
	return (
		<PageAnimation outEffect={selected === null}>
			<div className="mouse-outer" ref={outer}>
				<div style={innerStyle} className="mouse-inner" ref={inner}>
					{gallery
						.filter((_, id) => id < 12)
						.map((item, id) => (
							<Link key={id} to={`/galereya/${id}`}>
								<motion.div
									onClick={() => setSelected(id)}
									className="floating-pics"
									style={{
										top:
											layout[id].top *
											(wSize.width > 992 ? 1 : 0.66),
										left:
											layout[id].left *
											(wSize.width > 992 ? 1 : 0.66),
										zIndex: selected === id ? 2 : 1,
									}}
									initial={{ opacity: 0 }}
									animate={{
										opacity: 1,
										transition: { delay: id * 0.2 },
									}}
									exit={
										selected === id
											? {
													top:
														wSize.width > 992
															? innerOffset[1]
															: outer.current
																	?.scrollTop +
															  90,
													left:
														wSize.width > 992
															? innerOffset[0]
															: outer.current
																	?.scrollLeft +
															  94,
													width: "100vw",
													height: "80vh",
													transition: { duration: 1 },
											  }
											: null
									}
								>
									<img
										src={item.img}
										alt=""
										className="w-100 h-100"
									/>
								</motion.div>
							</Link>
						))}
				</div>
			</div>
		</PageAnimation>
	);
}
