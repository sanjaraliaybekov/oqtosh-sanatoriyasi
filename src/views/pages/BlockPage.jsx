import React, { useEffect, useState } from "react";
import { motion } from "framer-motion/dist/framer-motion";
import Header from "../../components/common/headers/header";
import { blocks } from "../../api/requests";

export default function BlockPage({ id }) {
	window.scrollTo({ top: 0, behavior: "smooth" });
	const [block, setBlock] = useState();
	useEffect(() => {
		blocks().then((res) => {
			setBlock(res.data);
		});
	}, []);

	if (!block) {
		return "";
	}
	return (
		<>
			<Header />
			<h1 className="text-center mb-5">{block[id].title}</h1>
			<motion.div
				className="bigImg"
				layoutId={block[id].link}
				transition={{ duration: 1 }}
				// style={{backgroundImage:`url("${block[id].image}")`}}
			>
				<img
					className="w-100"
					src={block[id].image}
					style={{ objectFit: "cover" }}
					alt="oqtosh"
				/>
			</motion.div>
			<h2 className="container mt-5 pt-4">{block[id].content}</h2>
		</>
	);
}
