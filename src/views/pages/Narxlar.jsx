import React, { useEffect, useState } from "react";
import { narxlar } from "../../api/requests";
import Header from "../../components/common/headers/header";
import PageAnimation from "../../components/common/PageAnimation";

export default function Narxlar() {
	const image = "/assets/images/prices.jpg";
	const [narx, setNarx] = useState([]);
	useEffect(() => {
		narxlar().then((res) => {
			setNarx(res.data);
		});
	}, []);

	const izoh =
		"Izoh: Axborot va kommunikatsiyalarni rivojlantirish vazirligi tizimiga kiruvchi korxonalar xodimlarining hordiq chiqarishini tashkil etish maqsadida preyskurant ishlab chiqilgan. Agar elektr energiyasi, gaz va boshqa moddiy xarajatlar ko'tarilsa, narxlar qayta ko'rib chiqilishi mumkin. Bir yil muddatga shartnoma tuzilib, shu yil 31-dekabrgacha 100% oldindan to‘lov amalga oshirilgan taqdirda, shu yil uchun preyskurant qo‘llaniladi.";
	return (
		<PageAnimation>
			<div className="narxlar">
				<div className="container">
					<Header />
				</div>
				<h1 className="text-center mb-5">Narxlar</h1>
				<img src={image} className="block-img" alt="oqtosh" />
				<div className="container">
					<div className="row">
						<div className="col-12">
							<h2 className="my-5">{izoh}</h2>
							<table className="table">
								<thead>
									<tr>
										<th scope="col">Xizmat turi</th>

										<th scope="col">Narx</th>

										<th scope="col">Qo'shimcha</th>
									</tr>
								</thead>
								<tbody>
									{narx.map((item, i) => (
										<tr key={i}>
											<td>{item.xizmat}</td>
											<td>{item.narx}</td>
											<td>{item.eslatma}</td>
										</tr>
									))}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</PageAnimation>
	);
}
