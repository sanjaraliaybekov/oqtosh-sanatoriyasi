import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react/swiper-react.js";
import { EffectCoverflow, Mousewheel } from "swiper";
import { Link } from "react-router-dom";
import "swiper/modules/pagination/pagination.scss";
import "swiper/modules/effect-coverflow/effect-coverflow.scss";
import "swiper/swiper.scss";
import PageAnimation from "../../components/common/PageAnimation";
import { shifokorlar } from "../../api/requests";
import Header from "../../components/common/headers/header";
export default function Shifokorlar() {
	const [specialist, setSpecialist] = useState();

	useEffect(() => {
		shifokorlar().then((res) => {
			setSpecialist(res.data);
		});
	}, []);

	if (!specialist) {
		return "";
	}

	return (
		<PageAnimation>
			<div className="container">
				<Header />
			</div>
			<h1 className="text-center mb-5 py-5">Shifokorlar</h1>
			<Swiper
				effect={"coverflow"}
				grabCursor={true}
				loop={true}
				// slidesPerGroup={3}
				centeredSlides={true}
				slidesPerView={3}
				coverflowEffect={{
					rotate: 50,
					stretch: 0,
					depth: 100,
					modifier: 1,
					slideShadows: true,
				}}
				direction={"horizontal"}
				mousewheel={true}
				pagination={true}
				spaceBetween={200}
				modules={[EffectCoverflow, Mousewheel]}
				className="py-5 my-5"
			>
				{specialist.map((item, id) => (
					<SwiperSlide key={id}>
						<Link to={`/shifokorlar/${id + 1}`}>
							<img
								src={item.image}
								className="swiper-card"
								height={500}
								alt=""
							/>
							<div className="swiper-text">
								<h2 className="text-center mb-4">
									{item.name}
								</h2>
								<p>{item.specialistic}</p>
							</div>
						</Link>
					</SwiperSlide>
				))}
			</Swiper>
		</PageAnimation>
	);
}
