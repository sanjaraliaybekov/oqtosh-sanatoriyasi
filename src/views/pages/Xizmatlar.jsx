import React, { useEffect, useState } from "react";
import { xizmatlar } from "../../api/requests";
import Header from "../../components/common/headers/header";
import PageAnimation from "../../components/common/PageAnimation";

export default function Xizmatlar() {
	const [xizmat, setXizmat] = useState([]);

	useEffect(() => {
		xizmatlar().then((res) => {
			setXizmat(res.data);
		});
	}, []);

	const image = "/assets/images/services.jpg";
	const content =
		"OQTOSH\" sanatoriyasi tog'li iqlim kurortidir. Hudud bargli o'rmonli tog 'yonbag'irlari bilan o'ralgan. Uning yonidan Oqtosh va Ayubsoy tog 'daryolari oqib o'tadi.";

	return (
		<PageAnimation>
			<div className="xizmatlar">
				<div className="container">
					<Header />
				</div>
				<h1 className="text-center mb-5">Xizmatlar</h1>
				<img src={image} alt="oqtosh" />
				<div className="container">
					<div className="row">
						<div className="col-12">
							<h2 className="my-5">{content}</h2>
							{xizmat.map((item, index) => (
								<div className="content" key={index}>
									<h3 className="title">
										<b>{item.title}</b>
									</h3>
									<ul>
										{item.xizmat.map((item, i) => (
											<li key={i}>
												<h3>{item.name}</h3>
											</li>
										))}
									</ul>
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		</PageAnimation>
	);
}
