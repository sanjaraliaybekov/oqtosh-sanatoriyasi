import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { shifokorlar } from "../../api/requests";
import PageAnimation from "../../components/common/PageAnimation";

export default function ShifokorlarItem() {
	let { id } = useParams();

	const [specialist, setSpecialist] = useState();

	useEffect(() => {
		shifokorlar().then((res) => {
			setSpecialist(res.data);
		});
	}, []);

	if (!specialist) {
		return "";
	}
	const special = specialist[id - 1];
	console.log(special);

	return (
		<PageAnimation>
			<div className="container shifokorlar-page">
				<div className="row">
					<div className="col-12">
						<Link to="/shifokorlar" className="my-5">
							<img
								src="/assets/images/icons/arrow.svg"
								alt="back"
							/>
						</Link>
					</div>
				</div>
				<div className="row">
					<div className="col-6 d-flex justify-content-center flex-column">
						<h2 className="name">{special.name}</h2>
						<h3>
							<span>Mutaxasisligi:</span> {special.specialistic}
						</h3>
						<h3>
							<span>Ma'lomoti:</span> {special.degree}
						</h3>
						<h3>
							<span>Ta'lim:</span> {special.study}
						</h3>
						<h3>
							<span>Tajriba: </span> 
							{special.work_experience}
						</h3>

						<ul>
							<h3>
								<span>Ish vaqti:</span>
							</h3>
							{special?.work_graphics.map((item, index) => (
								<li key={index}>
									<h3>{item}</h3>
								</li>
							))}
						</ul>
					</div>
					<div className="col-6">
						<div
							className="head-image"
							style={{
								width: "100%",
								height: "80vh",
								backgroundPosition: "center",
								backgroundSize: "cover",
								backgroundImage: `url("${
									specialist[id - 1]?.image
								}")`,
							}}
						/>
					</div>
				</div>
			</div>
		</PageAnimation>
	);
}
