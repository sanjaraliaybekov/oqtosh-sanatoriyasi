import React, { useEffect, useState } from "react";
import { eslatmalar } from "../../api/requests";
import Header from "../../components/common/headers/header";
import PageAnimation from "../../components/common/PageAnimation";

export default function Eslatmalar() {
	const [eslatma, setEslatma] = useState({ image: "", content: "" });

	useEffect(() => {
		eslatmalar().then((res) => {
			setEslatma({ image: res.data.image, content: res.data.content });
		});
	}, []);
	return (
		<PageAnimation>
			<div className="eslatma">
				<div className="container">
					<Header />
				</div>
				<h1 className="text-center mb-5">Eslatmalar</h1>
				<img src={eslatma.image} className="block-img" alt="oqtosh" />
				<div className="container">
					<div className="row">
						<div className="col-12">
							<h2 className="my-5">{eslatma.content}</h2>
						</div>
					</div>
				</div>
			</div>
		</PageAnimation>
	);
}
