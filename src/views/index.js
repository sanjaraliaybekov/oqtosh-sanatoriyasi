import React from "react";
import {Route, Switch, useLocation} from "react-router-dom";
import {AnimatePresence, AnimateSharedLayout} from "framer-motion/dist/framer-motion";
import Loader from "../components/features/loaders/Loader";
import LoaderOverlay from "../components/features/loaders/LoaderOverlay";
import Layout from "../components/layout/Layout";
import market from "../config/market";
import Home from "./home";
import NotFound from "./pages";
import BlockPage from "./pages/BlockPage";
import Eslatmalar from "./pages/Eslatmalar";
import Narxlar from "./pages/Narxlar";
import Shifokorlar from "./pages/Shifokorlar";
import Test from "./pages/Test";
import Xizmatlar from "./pages/Xizmatlar";
import ShifokorlarItem from "./pages/ShifokorlarItem";
import Galereya from "./pages/Galereya";
import GalleryItem from "./pages/GalleryItem";

export default function AppRoot() {
    const location = useLocation()
    return (
        <React.Suspense
            fallback={
                market.config.loader ? (
                    <Loader/>
                ) : (
                    <LoaderOverlay wrapper={true}/>
                )
            }
        >
            <Layout>
                <AnimateSharedLayout type='crossfade'>
                    <AnimatePresence exitBeforeEnter={true}>
                        <Switch location={location} key={location.pathname}>
                            <Route exact path="/" component={Home}/>
                            <Route path="/test" component={Test}/>

                            <Route path="/biz-haqimizda" component={() => <BlockPage id={0}/>}/>
                            <Route path="/tabiat" component={() => <BlockPage id={1}/>}/>
                            <Route path="/yotoqxonalar" component={() => <BlockPage id={2}/>}/>
                            <Route path="/tibbiyot" component={() => <BlockPage id={3}/>}/>
                            <Route path="/xizmatlar" component={Xizmatlar}/>
                            <Route path="/narxlar" component={Narxlar}/>
                            <Route exact path="/shifokorlar" component={Shifokorlar}/>
                            <Route path="/shifokorlar/:id" component={ShifokorlarItem}/>
                            <Route path="/eslatmalar" component={Eslatmalar}/>
                            <Route exact path="/galereya" component={Galereya}/>
                            <Route path="/galereya/:id" component={GalleryItem}/>

                            <Route component={NotFound}></Route>
                        </Switch>
                    </AnimatePresence>
                </AnimateSharedLayout>
            </Layout>
        </React.Suspense>
    );
}
