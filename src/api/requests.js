import axios from "axios";
import { apiURL } from "../constants/constants";


export const xizmatlar = () => {
	return axios.get(`${apiURL}/xizmatlar`);
};

export const narxlar = () => {
	return axios.get(`${apiURL}/narxlar`);
};
export const shifokorlar = () => {
	return axios.get(`${apiURL}/shifokorlar`);
};
export const eslatmalar = () => {
	return axios.get(`${apiURL}/eslatmalar`);
};
export const galereya = () => {
	return axios.get(`${apiURL}/galereya`);
};
export const izohlar = () => {
	return axios.get(`${apiURL}/izohlar`);
};
export const blocks = () => {
	return axios.get(`${apiURL}/blocks`);
};

export const address = () => {
	return axios.get(`${apiURL}/address`);
};
